<%-- 
    Document   : cadastro
    Created on : 17/10/2017, 19:39:41
    Author     : Administrador
--%>

<%@page import="br.com.senac.modelo.Cidade"%>
<jsp:include page="../header.jsp" />

<form class="form-horizontal">

<fieldset>
<div class="panel panel-primary">
    <div class="panel-heading">Cadastro de Cliente</div>
    
    <div class="panel-body">
<div class="form-group">


</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-2 control-label" for="Codigo">Codigo </label>  
  <div class="col-md-2">
  <input id="Nome" name="Codigo" placeholder="Codigo" class="form-control input-md"  type="text">
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="Nome">Nome </label>  
  <div class="col-md-5">
  <input id="Nome" name="Nome" placeholder="Escreva o Nome" class="form-control input-md" type="text">
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="Sobrenome">Sobrenome </label>  
  <div class="col-md-5">
  <input id="Nome" name="Sobrenome" placeholder="Escreva o Sobrenome" class="form-control input-md"  type="text">
  </div>
</div>




<!-- Prepended text-->
<div class="form-group">
  <label class="col-md-2 control-label" for="prependedtext">Telefone </label>
  <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
      <input id="prependedtext" name="prependedtext" class="form-control" placeholder="XX XXXXX-XXXX" type="text" maxlength="13" pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
      OnKeyPress="formatar('## #####-####', this)">
    </div>
  </div>
  
    <label class="col-md-1 control-label" for="prependedtext">Telefone</label>
     <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
      <input id="prependedtext" name="prependedtext" class="form-control" placeholder="XX XXXXX-XXXX" type="text" maxlength="13"  pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
      OnKeyPress="formatar('## #####-####', this)">
    </div>
  </div>
 </div> 

<!-- Prepended text-->
<div class="form-group">
  <label class="col-md-2 control-label" for="prependedtext">Email <h11>*</h11></label>
  <div class="col-md-5">
    <div class="input-group">
      <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
      <input id="prependedtext" name="prependedtext" class="form-control" placeholder="email@email.com" type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" >
    </div>
  </div>
</div>


<!-- Search input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="CEP">CEP <h11>*</h11></label>
  <div class="col-md-2">
    <input id="cep" name="cep" placeholder="Apenas n�meros" class="form-control input-md" value="" type="search" maxlength="8" pattern="[0-9]+$">
  </div>
 
</div>

<!-- Prepended text-->
<div class="form-group">
  <label class="col-md-2 control-label" for="prependedtext">Endere�o</label>
  <div class="col-md-4">
    <div class="input-group">
      <span class="input-group-addon">Rua</span>
      <input id="rua" name="rua" class="form-control" placeholder=""  readonly="readonly" type="text">
    </div>
    
  </div>
    <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon">Complemento <h11>*</h11></span>
      <input id="Complemento" name="Complemento" class="form-control" placeholder=""  type="text">
    </div>
    
  </div>
  
  <div class="col-md-3">
    <div class="input-group">
      <span class="input-group-addon">Bairro</span>
      <input id="bairro" name="bairro" class="form-control" placeholder="" readonly="readonly" type="text">
    </div>
    
  </div>
</div>


                                <div class="form-group">
  <label class="col-md-2 control-label" for="prependedtext"></label>
  <div class="col-md-4">
    <div class="input-group">
      <span class="input-group-addon">Cidade</span>
      <%--<input id="cidade" name="cidade" class="form-control" placeholder="" required=""  readonly="readonly" type="text"> --%>
      <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Selecione
                                    <span class="caret"></span>
                                </button>
      
     
      
    </div>
    
  </div>
  
   <div class="col-md-2">
    <div class="input-group">
      <span class="input-group-addon">Pa�s</span>
      <input id="pais" name="pais" class="form-control" placeholder="" required=""  readonly="readonly" type="text">
      
      
    </div>
    
  </div>
</div>


  
 
    
  </div>
</div>

 
 
 




  




<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-2 control-label" for="Salvar"></label>
  <div class="col-md-8">
    <button id="Cadastrar" name="Salvar" class="btn btn-success" type="Submit">Salvar</button>
    <button id="Cancelar" name="Cancelar" class="btn btn-danger" type="Reset">Cancelar</button>
  </div>
</div>



</fieldset>
    
    </form>